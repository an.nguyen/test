package hcmut.edu.vn;

import java.util.Properties;
import java.io.InputStream;
import java.sql.*;
import java.security.*;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit test for Demo App.
 */
public class AppTest {

    @Test
    public void testConnectDatabase() throws Exception {       
        Properties prop = new Properties();
        InputStream input = null;
        input = getClass().getResourceAsStream("/database.properties");         
        prop.load(input);

        String JDBC_DRIVER = prop.getProperty("db.driver");  
        String DB_URL = prop.getProperty("db.url");
                    
        String DB_USER = prop.getProperty("db.username");
        String DB_PASS = prop.getProperty("db.password");

        Class.forName(JDBC_DRIVER);
        Connection conn = null;
        conn = DriverManager.getConnection(DB_URL,DB_USER, DB_PASS);
        assertTrue("Connect to database sucessfully", conn != null);
    }

}
