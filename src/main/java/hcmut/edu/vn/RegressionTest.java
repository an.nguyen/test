package hcmut.edu.vn;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import org.testng.annotations.*;
import static org.testng.Assert.*;

import org.apache.commons.io.FileUtils;

import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;

public class RegressionTest {
	public WebDriver driver;
	private String version;
	   
	@Parameters("browser")
	@BeforeTest
	public void launchapp(String browser) throws MalformedURLException, IOException {
		  
	    if (browser.equalsIgnoreCase("firefox")) {
	        System.out.println(" Executing on Firefox");

	        Properties prop = new Properties();
			InputStream input = getClass().getResourceAsStream("/database.properties");			
			prop.load(input);
			version = prop.getProperty("pj.version");
			
			DesiredCapabilities capability = DesiredCapabilities.firefox();
		    driver = new RemoteWebDriver(new URL("http://selenium-grid.router.defaul.svc.cluster.local/wd/hub"), capability);

	        // Puts an Implicit wait, Will wait for 10 seconds before throwing exception
	        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	        driver.manage().window().maximize();
	    }
	    else {
	        throw new IllegalArgumentException("The Browser Type is Undefined");
	    }
	}
	
	@Parameters("browser")  
	@Test
	public void getversion(String browser) throws Exception {
	    String URL = "http://demo-at.example.com";
	        
	    // Launch website
	    
	    driver.get(URL);
		      
		driver.findElement(By.id("username")).sendKeys("admin1");
		    
		driver.findElement(By.id("pwd")).sendKeys("pasacctosit");
		      
		driver.findElement(By.className("btn-submit")).click();
		      
		try{
			getscreenshot(browser);
		   	assertEquals(driver.findElement(By.id("version")).getText(), version);
		}catch(Exception e){
		   	getscreenshot(browser);
		}
	}
	
	/*@Test
    public void testGoogleSearch() {
        // Optional, if not specified, WebDriver will search your path for chromedriver.
        //System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        //System.setProperty("webdriver.firefox.marionette","/home/annguyen/Project/selenium/geckodriver");
        //WebDriver driver = new ChromeDriver();

	    DesiredCapabilities capability = DesiredCapabilities.firefox();
		driver = new RemoteWebDriver(new URL("http://selenium-grid./wd/hub"), capability);
		
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
        driver.get("http://www.google.com.vn");
    }*/
	   
	@AfterTest
	public void closeBrowser() {
	    driver.quit();
	}

	public void getscreenshot(String browser) throws Exception {
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        String screenshot_path = System.getProperty("user.dir") + "/testoutput/screenshot_regressiontest" + browser + ".png";
        FileUtils.copyFile(scrFile, new File(screenshot_path));
    }
}
